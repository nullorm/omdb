import { shallow } from 'enzyme'
import React from 'react'

import { MovieList } from './MovieList'

it('renders not asked', () => {
  const searchResult = { data: [], notAsked: true }
  const wrapper = shallow(<MovieList moviesSearchResults={searchResult} />)
  expect(wrapper.text()).toBe('Start typing in the search bar')
})

it('renders typing', () => {
  const searchResult = { data: [], notAsked: true, typing: true }
  const wrapper = shallow(<MovieList moviesSearchResults={searchResult} />)
  expect(wrapper.text()).toBe('typing...')
})

it('renders loading', () => {
  const searchResult = { data: [], loading: true }
  const wrapper = shallow(<MovieList moviesSearchResults={searchResult} />)
  expect(wrapper.text()).toBe('Loading...')
})

it('renders data', () => {
  const searchResult = {
    data: [
      {
        Poster:
          'https://images-na.ssl-images-amazon.com/images/M/MV5BOWRjMDk1NTQtZTE0Zi00MjQ4LTk5ZjktNzNiZmVmMWEzYTJhXkEyXkFqcGdeQXVyMDI2NDg0NQ@@._V1_SX300.jpg',
        Title: '55 Days at Peking',
        Type: 'movie',
        Year: '1963',
        imdbID: 'tt0056800'
      },
      {
        Poster:
          'https://images-na.ssl-images-amazon.com/images/M/MV5BMzE3NjlkOGUtY2QzZS00ZjRlLWE1MzItN2UwN2JmMzQyNDRmXkEyXkFqcGdeQXVyMjM3NTA4NDk@._V1_SX300.jpg',
        Title: 'Number 55',
        Type: 'movie',
        Year: '2014',
        imdbID: 'tt2993698'
      },
      {
        Poster: 'https://images-na.ssl-images-amazon.com/images/M/MV5BMTAzNTI3NjI3MzVeQTJeQWpwZ15BbWU4MDA4MTg2MTMx._V1_SX300.jpg',
        Title: '55 esaaf',
        Type: 'movie',
        Year: '2001',
        imdbID: 'tt0499180'
      },
      {
        Poster:
          'https://images-na.ssl-images-amazon.com/images/M/MV5BYzFjNDgwNDYtM2RkMy00NzhkLTlkZjYtNzdmOTJkNzhhOGJkL2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyNjY1MTg4Mzc@._V1_SX300.jpg',
        Title: "Mr. & Mrs. '55",
        Type: 'movie',
        Year: '1955',
        imdbID: 'tt0048392'
      }
    ],
    page: 1
  }
  const wrapper = shallow(<MovieList moviesSearchResults={searchResult} />)
  expect(wrapper.find('.results').children().length).toBe(searchResult.data.length)
})
