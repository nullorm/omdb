import { combineReducers } from 'redux'

import movies from './movies/movies.reducer'

export const rootReducer = combineReducers({ movies })
