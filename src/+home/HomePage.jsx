import * as React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

import { actionCreator } from '../store'
import MovieDialog from '../components/MovieDialog'
import MovieList from '../components/MovieList'

export class HomePage extends React.Component {
  componentDidMount() {
    this.props.dispatch(actionCreator.SEARCH({ s: 'aa' }))
  }

  componentWillUnmount() {
    this.props.dispatch(actionCreator.SEARCH())
  }

  render() {
    return (
      <div className="home-page">
        <MovieDialog />
        <nav className="nav">
          <Link to="/search">Go to Search</Link>
        </nav>
        <MovieList />
      </div>
    )
  }
}

export default connect()(HomePage)
