import { Observable } from 'rxjs/Observable'

const baseURI = 'http://localhost:3001'

const headers = { 'Content-Type': 'application/json' }

const errorHandler = data => Observable.throw(new Error(data.error || data))

export default ({ body, method, url }) =>
  Observable.ajax({ body, crossDomain: true, headers, method, url: `${baseURI}/${url}` })
    .map(({ response }) => response.data)
    .catch(errorHandler)
