import reducer, { initialState } from './movies.reducer'
import * as types from '../actionTypes'

it('SELECT_MOVIE', () => {
  const action = { payload: { movie: { Title: 'lol' } }, type: types.SELECT_MOVIE }
  const state = reducer(initialState, action)
  expect(state.selectedMovie.metadata).toEqual(action.payload.movie)
  expect(state.selectedMovie.loading).toBe(true)
})

it('FETCH_MOVIE_DATA_FAIL', () => {
  const action = { payload: { error: 'lol' }, type: types.FETCH_MOVIE_DATA_FAIL }
  const state = reducer(initialState, action)
  expect(state.selectedMovie.error).toBe(action.payload.error)
})

it('FETCH_MOVIE_DATA_SUCCESS', () => {
  const action = { payload: { Actors: 'lol', Plot: 'plol' }, type: types.FETCH_MOVIE_DATA_SUCCESS }
  const state = reducer(initialState, action)
  expect(state.selectedMovie.detail).toEqual(action.payload)
})

it('SEARCH', () => {
  const action = { payload: { s: 'lol' }, type: types.SEARCH }
  const state = reducer(initialState, action)
  expect(state.searchResults).toEqual({ data: [], loading: true })
})

it('SEARCH FAIL', () => {
  const action = { payload: { error: 'lol' }, type: types.SEARCH_FAIL }
  const state = reducer(initialState, action)
  expect(state.searchResults).toEqual({ data: [], error: action.payload.error })
})

it('SEARCH SUCCESS', () => {
  const action = { payload: { data: ['lol'], page: 1 }, type: types.SEARCH_SUCCESS }
  const state = reducer(initialState, action)
  expect(state.searchResults).toEqual({ data: action.payload.data, page: action.payload.page })
})

it('UPDATE_SEARCH_TEXT', () => {
  const action = { payload: { searchText: 'lol' }, type: types.UPDATE_SEARCH_TEXT }
  const state = reducer(initialState, action)
  expect(state.searchResults.typing).toBe(true)
  expect(state.searchText).toBe(action.payload.searchText)
})
