import * as types from '../actionTypes'

export const initialState = {
  selectedMovie: { metadata: {}, notAsked: true },
  fetchedAllPages: false,
  searchResults: { data: [], notAsked: true },
  searchText: ''
}

export default function(state = initialState, action) {
  switch (action.type) {
    case types.SELECT_MOVIE: {
      const movie = (action.payload && action.payload.movie) || {}
      const selectedMovie = { loading: true, metadata: movie }
      return { ...state, selectedMovie }
    }
    case types.FETCH_MOVIE_DATA_FAIL: {
      const error = action.payload.error
      const selectedMovie = { error, metadata: state.selectedMovie.metadata }
      return { ...state, selectedMovie }
    }
    case types.FETCH_MOVIE_DATA_SUCCESS: {
      const detail = action.payload
      const selectedMovie = { detail, metadata: state.selectedMovie.metadata }
      return { ...state, selectedMovie }
    }
    case types.SEARCH: {
      const overrideData = !action.payload || !action.payload.page || action.payload.page === 1
      const data = overrideData ? [] : state.searchResults.data
      const searchResultState = action.payload ? { loading: true } : { notAsked: true }
      const searchResults = { data, ...searchResultState }
      return { ...state, searchResults }
    }
    case types.SEARCH_FAIL: {
      const searchResults = { data: [], error: action.payload.error }
      return { ...state, searchResults }
    }
    case types.SEARCH_SUCCESS: {
      const page = action.payload.page || 1
      const data = action.payload.data || []

      const searchResults = { data: page > 1 ? [...state.searchResults.data, ...data] : data || [], page }
      return { ...state, fetchedAllPages: !data.length, searchResults }
    }
    case types.UPDATE_SEARCH_TEXT: {
      const { searchText } = action.payload
      const searchResults = { ...state.searchResults, typing: true }
      return { ...state, searchResults, searchText }
    }
    default:
      return state
  }
}
