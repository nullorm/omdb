import * as React from 'react'
import { Card, Elevation } from '@blueprintjs/core'

export default ({ Poster: image, Title: title, Type: type, Year: year, imdbID, ...remainingProps }) => (
  <Card className="poster" elevation={Elevation.TWO} interactive {...remainingProps}>
    <img alt={title} height="150" src={image} width="100" />
    <header>
      <h5>{title}</h5>
    </header>
    <div className="poster__metadata">
      <div>
        #{type} - {year}
      </div>
    </div>
  </Card>
)
