import { shallow } from 'enzyme'
import React from 'react'

import { MovieDialog } from './MovieDialog'

it('renders loading', () => {
  const selectedMovie = { loading: true, metadata: { imdbID: 'q', Title: 'lol' } }
  const wrapper = shallow(<MovieDialog selectedMovie={selectedMovie} />)
  wrapper.unmount()
})

it('renders error', () => {
  const selectedMovie = { error: 'an error', metadata: { imdbID: 'q', Title: 'lol' } }
  const wrapper = shallow(<MovieDialog selectedMovie={selectedMovie} />)
  wrapper.unmount()
})

it('renders movie details', () => {
  const selectedMovie = { detail: { Actors: 'd', Plot: 'dfsd', Poster: 'pp' }, metadata: { imdbID: 'q', Title: 'lol' } }
  const wrapper = shallow(<MovieDialog selectedMovie={selectedMovie} />)
  wrapper.unmount()
})
