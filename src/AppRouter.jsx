import * as React from 'react'
import { Redirect, Route, Switch } from 'react-router'

// components
import HomePage from './+home/HomePage'
import SearchPage from './+search/SearchPage'

class App extends React.Component {
  render() {
    return (
      <Switch>
        <Route component={HomePage} path="/home" />
        <Route component={SearchPage} exact path="/search" />
        <Redirect from="/" to="/search" />
      </Switch>
    )
  }
}

export default App
