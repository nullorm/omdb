import { combineEpics } from 'redux-observable'

import moviesEpic from './movies/movies.epic'

export default combineEpics(moviesEpic)
