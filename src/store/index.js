import { applyMiddleware, compose, createStore } from 'redux'
import { createLogger } from 'redux-logger'
import { createEpicMiddleware } from 'redux-observable'

import rootEpic from './epic'
import { rootReducer } from './reducer'

export * from './action'

const devtoolsCompose = '__REDUX_DEVTOOLS_EXTENSION_COMPOSE__'
const isDev = process.env.NODE_ENV !== 'production'
const composeEnhancers = (isDev && window[devtoolsCompose]) || compose

const epic = createEpicMiddleware(rootEpic)
const logger = createLogger({ collapsed: true, duration: true })

function configureStore(initialState) {
  const middlewares = [epic, logger]
  const enhancer = composeEnhancers(applyMiddleware(...middlewares))
  return createStore(rootReducer, initialState, enhancer)
}

export const store = configureStore()
