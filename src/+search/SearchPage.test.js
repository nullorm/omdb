import { shallow } from 'enzyme'
import React from 'react'

import { SearchPage } from './SearchPage'

it('renders', () => {
  const searchResult = { data: [], notAsked: true }
  const searchText = 'lol'
  const wrapper = shallow(<SearchPage moviesSearchResults={searchResult} searchText={searchText} />)
  expect(wrapper.find('input').props().value).toBe(searchText)
})
