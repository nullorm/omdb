import { combineEpics } from 'redux-observable'
import { Observable } from 'rxjs/Observable'

import * as types from '../actionTypes'
import { actionCreator } from '../action'
import * as omdbAPI from '../../api/omdb.api'

const fetchMovieDetail = action$ =>
  action$
    .ofType(types.SELECT_MOVIE)
    .map(action => action.payload)
    .filter(payload => payload)
    .switchMap(payload =>
      omdbAPI
        .fetchDetails(payload.movie)
        .map(actionCreator.FETCH_MOVIE_DATA_SUCCESS)
        .catch(error => Observable.of(actionCreator.FETCH_MOVIE_DATA_FAIL({ error: error.message })))
    )

const search = action$ =>
  action$
    .ofType(types.SEARCH)
    .map(action => action.payload)
    .filter(payload => payload)
    .switchMap(payload =>
      omdbAPI
        .search(payload)
        .map(({ Search: data }) => actionCreator.SEARCH_SUCCESS({ data, page: payload.page }))
        .catch(error => Observable.of(actionCreator.SEARCH_FAIL({ error: error.message })))
    )

const searchNextPage = (action$, state) =>
  action$
    .ofType(types.SEARCH_NEXT_PAGE)
    .map(() => state.getState().movies)
    .filter(m => !m.searchResults.loading && !m.fetchedAllPages)
    .map(({ searchText, searchResults }) => actionCreator.SEARCH({ s: searchText, page: searchResults.page + 1 }))

const updateText = action$ =>
  action$
    .ofType(types.UPDATE_SEARCH_TEXT)
    .debounceTime(1000)
    .map(action => action.payload)
    .map(({ searchText }) => actionCreator.SEARCH({ s: searchText }))

export default combineEpics(fetchMovieDetail, search, searchNextPage, updateText)
