import request from './request'

export const fetchDetails = ({ imdbID }) => request({ method: 'get', url: `omdb/details/${imdbID}` })

export const search = ({ page, s }) => request({ method: 'get', url: `omdb/search/?s=${s}&page=${page || 1}` })
