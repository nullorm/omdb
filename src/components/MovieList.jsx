import * as React from 'react'
import { connect } from 'react-redux'

import Poster from './Poster'
import { actionCreator } from '../store'

export class MovieList extends React.Component {
  onOpenModal = movie => this.props.dispatch(actionCreator.SELECT_MOVIE({ movie }))

  render() {
    const searchResults = this.props.moviesSearchResults
    const typing = searchResults.typing && <div>typing...</div>
    const loading = searchResults.loading && <div>Loading...</div>
    if (searchResults.notAsked) {
      return typing || <div>Start typing in the search bar</div>
    } else if (searchResults.error) {
      return (
        <div>
          {typing}
          {searchResults.error}
        </div>
      )
    } else {
      const results = searchResults.data.map(movie => <Poster key={movie.imdbID} onClick={() => this.onOpenModal(movie)} {...movie} />)
      return (
        <div>
          {typing}
          <div className="results">{results}</div>
          {loading}
        </div>
      )
    }
  }
}

const mapStateToProps = state => ({
  moviesSearchResults: state.movies.searchResults
})

export default connect(mapStateToProps)(MovieList)
