import { shallow } from 'enzyme'
import React from 'react'

import Poster from './Poster'

it('renders', () => {
  const movieData = {
    Poster: 'https://ia.media-imdb.com/images/M/MV5BMTM5ODkwNjg1NF5BMl5BanBnXkFtZTcwNjA4MDY2Nw@@._V1_SX300.jpg',
    Title: 'UFC 55: Fury'
  }
  const wrapper = shallow(<Poster {...movieData} />)
  expect(wrapper.find('header').text()).toBe(movieData.Title)
  expect(wrapper.find('img').props().src).toBe(movieData.Poster)
})
