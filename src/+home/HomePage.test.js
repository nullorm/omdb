import { shallow } from 'enzyme'
import React from 'react'

import { HomePage } from './HomePage'

it('renders', () => {
  const dispatch = () => {}
  const wrapper = shallow(<HomePage dispatch={dispatch} />)
  expect(wrapper.find('nav').children().length).toBe(1)
})
