import * as React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

import { actionCreator } from '../store'

import MovieDialog from '../components/MovieDialog'
import MovieList from '../components/MovieList'

export class SearchPage extends React.Component {
  onSearch = event => this.props.dispatch(actionCreator.UPDATE_SEARCH_TEXT({ searchText: event.target.value }))

  onScroll = event => {
    const { clientHeight, scrollTop, scrollHeight } = event.target
    const currentHeight = clientHeight + scrollTop
    if (currentHeight > scrollHeight * 0.8) {
      this.props.dispatch(actionCreator.SEARCH_NEXT_PAGE())
    }
  }

  render() {
    return (
      <div className="search-page">
        <MovieDialog />
        <nav className="nav">
          <Link to="/home">Go to Home</Link>
        </nav>
        <div className="search__bar">
          <input
            className="pt-input search__bar__input"
            onChange={this.onSearch}
            placeholder="search movies/tv series"
            value={this.props.searchText}
          />
        </div>
        <div className="search__results" onScroll={this.onScroll}>
          <MovieList />
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  searchText: state.movies.searchText
})

export default connect(mapStateToProps)(SearchPage)
