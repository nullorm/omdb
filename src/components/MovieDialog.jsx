import * as React from 'react'
import { connect } from 'react-redux'
import { Dialog } from '@blueprintjs/core'

import { actionCreator } from '../store'

export class MovieDialog extends React.Component {
  onCloseModal = () => this.props.dispatch(actionCreator.SELECT_MOVIE())

  renderMovieData() {
    const movie = this.props.selectedMovie
    if (movie.loading) {
      return <React.Fragment>Loading...</React.Fragment>
    } else if (movie.error) {
      return <React.Fragment>{movie.error}</React.Fragment>
    } else if (movie.detail) {
      return (
        <React.Fragment>
          <img alt={movie.metadata.Title} src={movie.detail.Poster} />
          <div className="movie-details__actors">{movie.detail.Actors}</div>
          <div className="movie-details__plot">{movie.detail.Plot}</div>
        </React.Fragment>
      )
    }
  }

  render() {
    const movie = this.props.selectedMovie

    return (
      <Dialog isOpen={movie.metadata.imdbID} onClose={() => this.onCloseModal()} title={movie.metadata.Title}>
        <div className="movie-details pt-dialog-body">{this.renderMovieData()}</div>
      </Dialog>
    )
  }
}

const mapStateToProps = state => ({
  selectedMovie: state.movies.selectedMovie
})

export default connect(mapStateToProps)(MovieDialog)
